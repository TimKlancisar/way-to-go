const languages = [
    {
        name: 'English',
        locale: 'en',
    },
    {
        name: 'Italiano',
        locale: 'it',
    },
    {
        name: 'Српски',
        locale: 'sr',
    },
];

export default languages;
