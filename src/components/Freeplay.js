import React from 'react';
import {Goban} from 'react-go-board';
import godash from 'godash';
import ControlBar from './ControlBar';

export default class Freeplay extends React.Component {
    constructor(props) {
        super(props);

        const {annotations = []} = this.props;

        this.state = {
            board: this.initBoard(),
            annotations: annotations.map(godash.sgfPointToCoordinate),
            counter: 0,
            color: godash.BLACK,
            ko: null,
            koWarning: false,
        };

        this.handleCoordinateClick = this.handleCoordinateClick.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    initBoard() {
        const {initWhite = [], initBlack = [], size = 19} = this.props;
        const blackBoard = godash.placeStones(
            new godash.Board(size),
            initBlack.map(godash.sgfPointToCoordinate),
            godash.BLACK,
        );

        return godash.placeStones(
            blackBoard,
            initWhite.map(godash.sgfPointToCoordinate),
            godash.WHITE,
        );
    }

    handleCoordinateClick(coordinate) {
        const legalMove = godash.isLegalMove(this.state.board, coordinate, this.state.color);
        const notKoMove = !coordinate.equals(this.state.ko);
        if (legalMove) {
            if (notKoMove) {
                this.setState({
                    board: godash.addMove(this.state.board, coordinate, this.state.color),
                    color: this.props.alternateColors ?
                        godash.oppositeColor(this.state.color) : this.state.color,
                    counter: this.state.counter + 1,
                    ko: this.props.alternateColors ?
                        godash.followupKo(this.state.board, coordinate, this.state.color) : null,
                    koWarning: false,
                });
            } else {
                this.setState({koWarning: true});
            }
        }
    }

    handleReset() {
        this.setState({
            board: this.initBoard(),
            counter: 0,
            color: godash.BLACK,
            ko: null,
            koWarning: false,
        });
    }

    render() {
        return <div className='board'>
            <Goban boardColor='#eda' board={this.state.board}
                annotations={this.state.annotations}
                onCoordinateClick={this.handleCoordinateClick}/>
            <ControlBar onReset={this.handleReset}/>
        </div>;
    }
}
