E' il turno di nero con due gruppi che stanno per essere circondati.

O le 3 pietre nere o le 3 pietre bianche saranno catturate.

Board::capture-race-1

Chiameremo questa situazione una **gara di cattura** dato che nessuno dei due
gruppi interni ha due occhi ed entrambi stanno gareggiando per catturare l'altro.

Nero è in grado di vincere la gara perchè è il turno di nero. Se fosse stato il turno
di bianco, avrebbe vinto lui la gara.

---

Nero può vincere la gara di cattura.

Board::capture-race-2

---

Salva le 3 pietre nere nell'angolo!

Board::capture-race-3
