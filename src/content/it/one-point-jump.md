In questa lezione, imparerai le mosse che vengono giocate quando nero
e bianco *non* si stanno attaccando direttamente fra loro.

Questo è il momento per giocare mosse che rinforzano la tua posizione, 
preparano per una futura battaglia, o fanno territorio. 

Board::one-point-jump

La pietra segnata è chiamata **salto di uno spazio**, o **ikken tobi**.
Le due pietre nere sono quasi connesse. E' comune vedere questa mossa
usata in molte situazioni come per esempio: attaccare, difendersi o scappare.

C'è un proverbio di Go che esprime la versatilità di questa mossa.

*Nessun salto da uno è una cattiva mossa.*

Essenzialmente, se non sai dove giocare, trova un salto da uno e giocalo.

---

Board::white-approach

Qui si può vedere un salto da uno come mossa difensiva in risposta
all'approccio avversario.

---

Board::double-approach

Se nero ignorasse la mossa bianca, l'angolo nero potrebbe essere attaccato
da entrambi i lati.