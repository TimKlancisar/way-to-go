### Komi

Nelle partite normali, nero gioca sempre la prima mossa. Perciò, nero
ha un leggero vantaggio su bianco. Per compensare questa ingiustizia,
a bianco viene di solito concesso un bonus di punti, chiamato Komi,
quando vengono contati i territori alla fine della partita. 

Questo bonus è di solito di 6.5 punti. Questo significa che il valore
della prima mossa è considerato essere pari a 6 punti e mezzo.
Il mezzo punto addizionale è stato introdotto per evitare i pareggi.

Per esempio, se nero ha 7 punti più di bianco, nero vince di 0.5 punti.
Se nero ha 6 punti più di bianco, bianco vince di 0.5 punti.

Ai vecchi tempi, non c'era il sistema del Komi. Quando sempre
più persone iniziarono a rendersi conto del vantaggio di nero, il Komi
fu introdotto.

Il valore del Komi è cambiato nel corso del tempo. Quando è stato introdotto
nelle partite dei professionisti giapponesi, era di 4.5 punti. Tuttavia,
nero aveva comunque migliori chance di vincere, così il Komi è stato
incrementato a 5.5 punti nel 1974. Nel 2002, l'Associazione Giapponese di Go
ha di nuovo aumentato il valore del Komi a 6.5.

Con il sistema del Komi, il Go non ha quasi mai partite che finiscono in
pareggio ed è diventato più eccitante ed equo

---

### La partita ad Handicap

Quando la forza di due giocatori è diversa, il giocatore più debole posiziona
alcune pietre come handicap prima che la partita cominci. Nelle partite ad
handicap, il giocatore più debole gioca sempre come nero e il giocatore
più forte come bianco.

Nero posiziona le pietre di handicap su dei punti prefissati e a quel punto
bianco gioca la prima mossa.

Il numero di pietre di handicap riflette la differenza fra i due giocatori.

Se il giocatore più debole è un 5 Kyu e il giocatore più forte è un 2 Kyu,
verranno usate 3 pietre.

Con le pietre di handicap, nero ha un vantaggio in ogni aspetto della partita -
attaccare, difendersi, circondare territorio, ecc.

Si dice che una pietra di handicap equivale a 10 punti di territorio.

Perciò, se giochi una partita alla pari con me e perdi di 50 punti, una partita
con 5 pietre di handicap sarebbe appropriata per dare ad entrambi un equa 
chance di vittoria.

La posizione convenzionale delle pietre di handicap è specificata qui sotto.

---

9 Handicap

Board::komi0

---

6 Handicap

Board::komi1

---

5 Handicap

Board::komi2

---

4 Handicap

Board::komi3

---

In ogni caso, le pietre di hadicap sono posizionate sui punti 4-4 (star point)
che sono evidenziati con un piccolo cerchio nero. Tuttavia, puoi avere
più di 9 pietre di handicap posizionandole altrove.
Col sistema ad handicap, chiunque può godersi una partita a Go con le stesse
possibilità di vittoria, a prescindere dalla differenza di forza fra i due
giocatori, senza alterare le regole. 
