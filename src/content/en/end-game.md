We're going to take a look at when a game ends here and how score is counted.
We'll use a 5x5 board so it's a bit easier to understand.

## Territory

Board::end-game-1

Both players made 5 moves each and the game is considered to be over.

Black has 5 points on the left and white has 10 points on the right, so white
wins by 5 points.

Open spaces, including corners and edges, are counted.  However, stones are not
counted.

---

A game is considered finished when both players decide they do not want to play
another move.

Why is the first figure considered over?

Board::end-game-2

If we continue the game, we can see that if black plays in white's territory,
white can easily capture the black stone.

---

On the other hand, if black decides to play within black's territory, the size
of that territory decreases.

Board::end-game-3

That said, note that sometimes it does make sense to play in your own territory
if you are looking to make two eyes for life or to strengthen your wall to
prevent an invasion.

---

This is also the end of a game.  We can count 5 points for black and 6 for
white.

Board::end-game-4

What about the two points in the middle?  These points are called **dame** -
neutral points awarded to neither player.

Either player may play there, but no points will be gained nor lost.

## When does a game end?

* When a player decides that they don't want to play anywhere, they can pass.
  If there are two successive passes, the game ends.  Territory is counted and
  added to captured pieces which gives us a player's total points.  The player
  with more points wins.
* When either player thinks they cannot win no matter how they play, they may
  resign.

Unlike most games, resignation is considered an honorable outcome.  Playing on
in the face of crushing defeat is not.
