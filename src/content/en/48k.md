## Capture anyway!

You can't always get success with only one move.

Get rid of the three white stones!

It takes two moves to do that.  The first move is important.

Board::capture1

---

Try to capture two white stones on the left edge.

It is hard to 'run away' with stones on the edge.

Board::capture2

---

So many stones! Just be careful which stones are in Atari.

If you make a wrong move, your stones will be killed instead!

Board::capture3
