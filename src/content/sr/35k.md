## Завршница партије

Како се партија приближава крају и када су територије црног и белог углавном
одређене, остаје само да се тачно одреди граница између територија.

У овој фази партије, мораш да погураш границу што више на противникову страну,
повећавајући своју и смањивајући противникову територију.

---

Црни је оградио леву, док бели има контролу над десном страном. Међутим, и даље
постоји пар места где граница није прецизно дефинисана.

Где треба да играш да би добио максималан профит?

Board::yose-1

---

Црни има леву а бели десну страну. Партија је скоро готова, али буди пажљив до
самог краја!

Бели је управо одиграо означени потез. Ако га игноришеш, твоја драгоцена
територија ће бити уништена.

Board::yose-2
